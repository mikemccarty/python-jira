import sys
from jira import JIRA

def generate_description(controller_name):
    """
    Notes: New integration testing framework that was sketched out in the PoC will be
        covered in the second pass.
    """
    return """
    We need to port the %s endpoints over to the Public API architecture.
    Create a versioned controller (with inheritance hierarchy if needed) and versioned RequestMappers.
    - Add Swagger annotations to each controller method exposed in the REST API.
    - Add new model class(es) for each end point.
    - Add Hateoas links to controllers and ResourceSupport base class to models.
    - Add Jackson versioning for accepted and returned payloads.
    - Fix existing unit / integration tests for each REST endpoint in the controller and fill gaps.
    -- Find areas where testing can be improved.
    - Do a quick security audit and create user stories to be addressed in the future.
    - Note areas in the controller code that should be refactored into something like a service.
    """ % controller_name

def create_story(controller_name):

    issue_dict = {
        'project': {'id': 10101},
        'summary': "[Public API] %s Controller" % controller_name,
        'description': generate_description(controller_name),
        'issuetype': {'name': 'Story'},
        'components': [{'name': 'Server: Rest API', 'id': '10401'}],
        'fixVersions': [{'name': 'MR5', 'id': '18600'}]
    }

    return jira.create_issue(fields=issue_dict)

def add_stories_to_epic(epic_id, issues):
    jira.add_issues_to_epic(epic, [i.key for i in issues])

def update_description(issue, controller_name):
    issue.update(description = generate_description(controller_name))

def find_issue(issues, controller_name):
    for i in issues:
        if controller_name in i.fields.summary:
            return i

def update_issues(issues, controllers):
    for c in controllers:
        issue = find_issue(issues, c)
        print("Updating:", issue, issue.fields.summary)
        issue.update(description = generate_description(c))


if __name__ == "__main__":
    username, password = sys.argv[1:]
    jira = JIRA(basic_auth=(username, password), server='https://zoomdata.atlassian.net')

    controllers = [
    'Users', 'Groups', 'Connections', 'Connectors', 'Sources', 'Bookmarks', 'Uploads',
    'Accounts', 'Activity', 'Attribute Storing', 'Branding', 'Data Export',
    'Formulas', 'Global Security', 'Job', 'Kerberos', 'LDAP', 'OAuth Connection',
    'SAML', 'Screenshot', 'Security Attributes',
    'Security Filters', 'Stream', 'System', 'Version', 'Visualization Defs',
    'Visualizations']

    new_controllers = [
    'IOAuth2ClientController', 'IOAuth2TokenController', 'Public Uploads'
    ]

    #For creating the user stories originally.
    #issues = list(map(create_story, controllers))
    issues = list(map(create_story, new_controllers))
    #add_stories_to_epic("ZD-28430", issues)

    #issues = jira.search_issues('"Epic Link" = ZD-28430')
    #update_issues(issues, controllers)
